# CloudServiceTask

This project is used to view rocket launches sorted by the latest date.
In project, you can filter launches by name, date, and by successful or unsuccessful starts.
Project also allow possibility to see more information and pictures of chosen launch.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

If you have any question contact with me via email: przemyslaw.woznica97@gmail.com
