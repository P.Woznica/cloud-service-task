import {Patch} from "./patch.interface";
import {Flickr} from "./flickr.interface";

export interface Links{
  patch: Patch,
  flickr: Flickr
}
