import {Links} from "./links.interface";

export interface LaunchesResponse {
  id: string,
  flight_number: number,
  name: string,
  date_local: Date,
  success: boolean,
  rocket: string,
  crew: string[],
  details: string,
  links: Links;
}
