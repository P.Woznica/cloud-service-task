import {Subscription} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable()
export class SubscriptionsContainer {
  private subscriptions: Subscription[] = [];
  constructor() {
  }

  set add(subscription: Subscription | undefined) {
    if (subscription === undefined) {
      console.error('Subscription in SubscriptionContainer is undefined');
      return;
    }
    this.subscriptions.push(subscription);
  }

  public dispose(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
