import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {RippleModule} from "primeng/ripple";
import {ButtonModule} from "primeng/button";
import {DividerModule} from "primeng/divider";
import {CheckboxModule} from "primeng/checkbox";
import {CalendarModule} from "primeng/calendar";
import {InputTextModule} from "primeng/inputtext";
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {GalleriaModule} from "primeng/galleria";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule,
    RippleModule,
    ButtonModule,
    DividerModule,
    CheckboxModule,
    CalendarModule,
    InputTextModule,
    ReactiveFormsModule,
    HttpClientModule,
    GalleriaModule
  ],
  exports: [
    CommonModule,
    RouterModule,
    RippleModule,
    ButtonModule,
    DividerModule,
    CheckboxModule,
    CalendarModule,
    InputTextModule,
    ReactiveFormsModule,
    HttpClientModule,
    GalleriaModule
  ]
})
export class SharedModule { }
