export enum LaunchesSearchFormEnum {
  SUCCESS = 'success',
  FLIGHT_DATE = 'flight_date',
  FLIGHT_NAME = 'flight_name',
}
