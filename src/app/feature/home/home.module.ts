import { NgModule } from '@angular/core';
import { DetailsComponent } from './details/details.component';
import {HomeComponent} from "./home.component";
import {SharedModule} from "../../shared/modules/shared.module";
import {HomeRoutingModule} from "./home-routing.module";
import {HomeService} from "./home.service";
import {HomeFilterService} from "./home-filter.service";


@NgModule({
  declarations: [
    HomeComponent,
    DetailsComponent
  ],
  imports: [
    SharedModule,
    HomeRoutingModule
  ],
  providers: [
    HomeService,
    HomeFilterService
  ]
})
export class HomeModule { }
