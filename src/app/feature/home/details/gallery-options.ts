export class GalleryOptions{
  public readonly responsiveGalleryOptions: any[] = [
    {
      breakpoint: '1366px',
      numVisible: 5
    },
    {
      breakpoint: '1030px',
      numVisible: 3
    },
    {
      breakpoint: '991px',
      numVisible: 2
    },
    {
      breakpoint: '767px',
      numVisible: 1
    }
  ];
}
