import {Component, OnDestroy, OnInit} from '@angular/core';
import {HomeService} from "../home.service";
import {ActivatedRoute} from "@angular/router";
import {RouterLinksEnum} from "../../../shared/enums/routerLinks.enum";
import {GalleryOptions} from "./gallery-options";
import {filter, mergeAll,} from "rxjs";
import {LaunchesResponse} from "../../../shared/interfaces/launches-response.interface";
import {SubscriptionsContainer} from "../../../shared/helpers/subscriptions-container";

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  providers: [GalleryOptions]
})
export class DetailsComponent implements OnInit, OnDestroy{
  public currentId!: string;
  public currentLaunch!: LaunchesResponse
  public readonly homeRouterReturn = RouterLinksEnum.RETURN
  private subscribeContainer: SubscriptionsContainer = new SubscriptionsContainer();

  constructor(
    public galleryOptions: GalleryOptions,
    private homeService: HomeService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getCurrentLaunchId();
    this.getCurrentLaunch();
  }

  ngOnDestroy(): void {
    this.subscribeContainer.dispose()
  }

  private getCurrentLaunch(): void{
    this.subscribeContainer.add = this.homeService.getLaunches()
      .pipe(
        mergeAll(),
        filter(response => response.id === this.currentId))
      .subscribe(
      {
        next: value => this.currentLaunch = value,
        error: err => console.error(err)
      }
    )
  }

  private getCurrentLaunchId(): void{
    this.subscribeContainer.add = this.route.queryParams
      .subscribe({
        next: param => this.currentId = param['currentId'],
        error: err => console.error(err)
      });
  }
}
