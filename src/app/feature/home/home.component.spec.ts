import {ComponentFixture, TestBed} from '@angular/core/testing';
import {HomeComponent} from './home.component';
import {HomeService} from './home.service';
import {FormBuilder} from '@angular/forms';
import {HomeFilterService} from './home-filter.service';
import {of} from 'rxjs';
import {LaunchesResponse} from '../../shared/interfaces/launches-response.interface';
import {DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let debugElement: DebugElement

  const dummyLaunchWithId = (id: string) => {
    return {
      id,
      flight_number: 12345,
      date_local: new Date(),
      name: 'test flight',
      success: true,
      rocket: 'test rocket',
      crew: ["test1", "test2"],
      details: "test details",
      links: {
        patch: {
          small: "test small value",
          large: "test large value"
        },
        flickr: {
          small: ["test small flickr"],
          original: ["test original flickr"]
        }
      }
    }
  };

  const generateLaunches = (amount: number): LaunchesResponse[] => {
    const launches = []
    for (let i = 0; i < amount; i++) {
      launches.push(dummyLaunchWithId('test_id' + i))
    }
    return launches
  }

  const homeServiceStub = {
    getLaunches: () => of(generateLaunches(100))
  }

  beforeEach(async () => {

    const homeFilterServiceStub = {}

    await TestBed.configureTestingModule({
      declarations: [HomeComponent],
      providers: [{provide: HomeService, useValue: homeServiceStub}, FormBuilder,
        {provide: HomeFilterService, useValue: homeFilterServiceStub}]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain 40 launches after method', () => {
    expect(component.launchesListToView.length).toEqual(20);
    expect(debugElement.queryAll((By.css('#launch-wrapper'))).length).toEqual(20)
    component.showMoreLaunches()
    fixture.detectChanges();
    expect(component.launchesListToView.length).toEqual(40);
    expect(debugElement.queryAll((By.css('#launch-wrapper'))).length).toEqual(40)
  });
});
