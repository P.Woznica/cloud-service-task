import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {LaunchesResponse} from "../../shared/interfaces/launches-response.interface";

@Injectable()

export class HomeService {
  private readonly launchesEndpoint = 'https://api.spacexdata.com/v4/launches';

  constructor(
    private httpClient: HttpClient
  ) { }

  public getLaunches(): Observable<LaunchesResponse[]>{
    return this.httpClient.get<LaunchesResponse[]>(this.launchesEndpoint)
  }
}
