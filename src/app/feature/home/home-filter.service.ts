import { Injectable } from '@angular/core';
import {LaunchesResponse} from "../../shared/interfaces/launches-response.interface";

@Injectable()
export class HomeFilterService {

  public filterWithoutDate(dataArray: LaunchesResponse[], onlySuccess: boolean, searchValue: string): LaunchesResponse[]{
    return dataArray.filter(launch => this.isNameEquals(launch.name, searchValue) && this.isCheckboxChecked(launch.success, onlySuccess))
  }

  public filterWithDate(dataArray: LaunchesResponse[], onlySuccess: boolean, startDate: Date, endDate: Date, searchValue: string): LaunchesResponse[]{
    return dataArray.filter(launch => this.isDateBetween(launch.date_local, startDate, endDate) && this.filterWithoutDate(dataArray, onlySuccess,searchValue))
  }

  private isNameEquals(launchName: string, searchValue: string): boolean{
    return launchName.startsWith(searchValue)
  }

  private isDateBetween(value: Date, startDate: Date, endDate: Date): boolean {
    return new Date(value).getTime() > startDate.getTime() && new Date(value).getTime() < endDate.getTime()
  }

  private isCheckboxChecked(success: boolean, onlySuccess: boolean): boolean {
    if (onlySuccess) {
      return success
    }
    return true
  }
}
