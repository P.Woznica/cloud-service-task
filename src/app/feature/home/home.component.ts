import {Component, OnDestroy, OnInit} from '@angular/core';
import {LaunchesSearchFormEnum} from "../../shared/enums/launches-search-form-enum";
import {FormBuilder, FormGroup} from "@angular/forms";
import {LaunchesResponse} from "../../shared/interfaces/launches-response.interface";
import {HomeService} from "./home.service";
import {RouterLinksEnum} from "../../shared/enums/routerLinks.enum";
import {HomeFilterService} from "./home-filter.service";
import {SubscriptionsContainer} from "../../shared/helpers/subscriptions-container";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  public readonly detailsRouterLink = RouterLinksEnum.DETAILS
  public readonly flightName = LaunchesSearchFormEnum.FLIGHT_NAME
  public readonly flightDate = LaunchesSearchFormEnum.FLIGHT_DATE
  public readonly success = LaunchesSearchFormEnum.SUCCESS
  public formGroup!: FormGroup;
  public launchesList: LaunchesResponse[] = [];
  public launchesListToView: LaunchesResponse[] = [];
  private subscribeContainer: SubscriptionsContainer = new SubscriptionsContainer();

  constructor(
    private homeService: HomeService,
    private formBuilder: FormBuilder,
    private homeFilterService: HomeFilterService
  ) {
  }

  ngOnInit(): void {
    this.formGroup = this.buildForm();
    this.getLaunchesList()
  }

  ngOnDestroy(): void {
    this.subscribeContainer.dispose()
  }

  public showMoreLaunches(): void {
    let newArrayLength = this.launchesListToView.length + 20;
    if (newArrayLength > this.launchesList.length) {
      newArrayLength = this.launchesList.length
    }
    this.launchesListToView = this.launchesList.slice(0, newArrayLength);
  }

  public onSubmit(): void {
    const searchValue = this.formGroup.controls[this.flightName].value
    const onlySuccess = this.formGroup.controls[this.success].value
    if (this.formGroup.controls[this.flightDate].value === null) {
      this.launchesListToView = this.homeFilterService.filterWithoutDate(this.launchesList, onlySuccess, searchValue)
    } else {
      const startDate = this.formGroup.controls[this.flightDate].value[0]
      const endDate = this.formGroup.controls[this.flightDate].value[1]
      this.launchesListToView = this.homeFilterService.filterWithDate(this.launchesList, onlySuccess, startDate, endDate, searchValue)
    }
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      [this.flightName]: [null],
      [this.flightDate]: [null],
      [this.success]: [null],
    });
  }

  private getLaunchesList(): void {
    this.subscribeContainer.add = this.homeService.getLaunches()
      .subscribe({
          next: launchesList => {
            this.launchesList = launchesList.sort(response =>
              new Date(response.date_local).getTime() - new Date().getTime())
            this.showMoreLaunches()
          },
          error: err => console.error(err)
        }
      )
  }
}
